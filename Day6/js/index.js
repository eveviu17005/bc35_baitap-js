function addTextbyId(id, text) {
    document.getElementById(id).innerText = text;
}

function findMin() {
    for (var step = 1, sum = 0, min = 0; sum < 1e+4; step++) {
        sum += step;
        min = step;
    }
    addTextbyId("txtFindMin", `Số nguyên dương nhỏ nhất: ${min}`)
}



function sumCalculate() {
    for (var step = 1, inputX = document.getElementById("inputX").value * 1, inputN = document.getElementById("inputN").value * 1, sum = 0; step <= inputN; step++) {
        sum += Math.pow(inputX, step);
    }
    addTextbyId("txtSumResult", `Tổng: ${sum}`)
}

function findFactorialOfANumber() {
    for (var step = 1, inputN1 = document.getElementById("inputN1").value * 1, productOfNumber = 1; step <= inputN1; step++) {
        productOfNumber *= step;

    }

    addTextbyId("txtFactorialResult", `Giai thừa: ${productOfNumber}`)
}

function creatDiv() {
    for (var step = 1, content = ""; step <= 10; step++) {
        if (step % 2 == 0) {
            content += `<div style ="Background: blue; color: white;">Div chẵn</div>`;
        }
        else {
            content += `<div style ="Background: red;color: white;">Div lẻ</div>`
        }

        document.getElementById("txtDivResult").innerHTML = content;
    }
}

function isPrimeNumber(inputNum) {
    for (var step = 2, count = 0; step <= inputNum; step++) {
        if (inputNum % step == 0) {
            count += 1;
        }
    }
    if (count > 2) {
        return false
    }
    else if (count == 1) {
        return true
    }
}

function printPrimeNumber() {
    var inputN2 = document.getElementById("inputN2").value * 1;

    if (inputN2 < 2) {
        addTextbyId("txtPrimeResult", "Hãy nhập vào số lớn hơn hoặc bằng 2")
    }
    else {
        for (var step = 2, content = ""; step <= inputN2; step++) {
            switch (isPrimeNumber(step)) {
                case true:
                    content += step + " "
                    break;
            }
        }
        addTextbyId("txtPrimeResult", `${content}`)
    }

}