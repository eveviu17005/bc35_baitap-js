function addTextById(id, text) {
    document.getElementById(id).innerText = text;
}

function sortThreeNumbers(num1, num2, num3) {
    // var biggest;

    // if (num1 >= num2 && num1 >= num3){
    //     biggest  = num1;
    // }
    // else if (num2 >= num1 && num2 >= num3){
    //     biggest = num2;
    // }
    // else if (num3 >= num1 && num3 >= num2){
    //     biggest = num3;
    // }
    // return biggest;
    var smallest = 0;
    var middle = 0;
    var largest = 0;

    if (num1 <= num2 && num1 <= num3) {
        smallest = num1;
        if (num2 <= num3) {
            middle = num2;
            largest = num3
        }
        else {
            middle = num3;
            largest = num2
        }
    }
    else if (num2 <= num1 && num2 <= num3) {
        smallest = num2;
        if (num1 <= num3) {
            middle = num1;
            largest = num3;
        }
        else {
            middle = num3;
            largest = num1;
        }
    }
    else if (num3 <= num1 && num3 <= num2) {
        smallest = num3;
        if (num1 <= num2) {
            middle = num1;
            largest = num2;
        }
        else {
            middle = num2;
            largest = num1;
        }
    }
    return [smallest, middle, largest];
}



function showSortNumber() {
    var inpuNum1 = document.getElementById("inputNum1").value * 1;
    var inputNum2 = document.getElementById("inputNum2").value * 1;
    var inputNum3 = document.getElementById("inputNum3").value * 1;
    var [smallest, middle, largest] = sortThreeNumbers(inpuNum1, inputNum2, inputNum3)

    document.getElementById("txt-sorting").innerText = `${smallest}<${middle}<${largest}`

}



function familyGreeting() {
    var select = document.getElementById('selUser');
    var value = select.options[select.selectedIndex].value;
    switch (value) {
        case "":
            addTextById("txt-greeting", "Không quen, cút!")
            break;
        case "B":
            addTextById("txt-greeting", "Xin chào Bố!")
            break;
        case "M":
            addTextById("txt-greeting", "Xin chào Mẹ!")
            break;
        case "A":
            addTextById("txt-greeting", "Xin chào Anh trai!")
            break;
        case "E":
            addTextById("txt-greeting", "Xin chào Em gái!")
            break;
    }
}

function isEven(input) {
    if (input % 2 == 0) {
        return true
    }
    else {
        return false
    }
}

const INPUT_AMOUNT = 3;
function evenOddNumbersCounting() {

    var inputCount1 = document.getElementById("inputCount1").value * 1;
    var inputCount2 = document.getElementById("inputCount2").value * 1;
    var inputCount3 = document.getElementById("inputCount3").value * 1;
    var even = 0;


    switch (isEven(inputCount1)) {
        case true:
            even += 1;
            break;

    }
    switch (isEven(inputCount2)) {
        case true:
            even += 1;
            break;

    }
    switch (isEven(inputCount3)) {
        case true:
            even += 1;
            break;
    }

    var odd = INPUT_AMOUNT - even

    addTextById("txt-even-odd", `Có ${even} số chẵn, ${odd} số lẻ`)
}

function isTriagnle() {

}

function predictTriangle() {
    var inputEdge1 = document.getElementById("inputEdge1").value * 1;
    var inputEdge2 = document.getElementById("inputEdge2").value * 1;
    var inputEdge3 = document.getElementById("inputEdge3").value * 1;
    var squareSumEdge12 = (inputEdge1 * inputEdge1) + (inputEdge2 * inputEdge2)
    var squareSumEdge13 = (inputEdge1 * inputEdge1) + (inputEdge3 * inputEdge3)
    var squareSumEdge23 = (inputEdge2 * inputEdge2) + (inputEdge3 * inputEdge3)
    var squareEdge1 = inputEdge1 * inputEdge1;
    var squareEdge2 = inputEdge2 * inputEdge2;
    var squareEdge3 = inputEdge3 * inputEdge3;

    if (inputEdge1 == inputEdge2 && inputEdge1 == inputEdge3 && inputEdge2 == inputEdge3) {
        addTextById("txt-predictTriangle", "Hình tam giác điều")
    }
    else if (inputEdge1 == inputEdge2 || inputEdge1 == inputEdge3 || inputEdge2 == inputEdge3) {
        addTextById("txt-predictTriangle", "Hình tam giác cân")
    }
    else if ((squareEdge1 == squareSumEdge23) || (squareEdge2 == squareSumEdge13) || (squareEdge3 == squareSumEdge12)) {
        addTextById("txt-predictTriangle", "Hình tam giác vuông")
    }
    else {
        addTextById("txt-predictTriangle", "Một loại tam giác nào đó ")
    }
}

function checkValidDate(day, month, year) {
    if (day <= 0 || day >= 32) {
        addTextById("txt-dateResult", "Ngày không xác định")
    }
    else if (month < 0 || month > 12) {
        addTextById("txt-dateResult", "Tháng không xác định")
    }
    else if (year <= 0) {
        addTextById("txt-dateResult", "Năm phải lớn hơn hoặc bằng 0")
    }


}
function isLeapYear(year) {
    if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
        return true;
    }
    return false;

}
function getNumberOfDaysFromMonth(month, year) {
    switch (month) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            return 31;
        case 4:
        case 6:
        case 9:
        case 11:
            return 30;
        case 2:
            switch (isLeapYear(year)) {
                case true:
                    return 29;
                case false:
                    return 28;

            }
    }
}



function nextDateCalculate() {
    var inputDay = document.getElementById("inputDay").value * 1;
    var inputMonth = document.getElementById("inputMonth").value * 1;
    var inputYear = document.getElementById("inputYear").value * 1;
    var lastDayOfMonth = getNumberOfDaysFromMonth(inputMonth, inputYear)
    var currentDay = inputDay;
    var currentMonth = inputMonth;
    var currentYear = inputYear;

    switch (isLeapYear(inputYear)) {
        case true:
            if (inputDay <= 0 || inputDay >= 32 || inputMonth <= 0 || inputMonth > 12 || inputYear <= 0) {
                checkValidDate(inputDay, inputMonth, inputYear)
            }
            else {
                if (inputDay > lastDayOfMonth) {
                    addTextById("txt-dateResult", "Ngày không xác định")
                }
                else if (inputDay == lastDayOfMonth && inputMonth == 12) {
                    currentDay = 1;
                    currentMonth = 1;
                    currentYear++;
                    addTextById("txt-dateResult", `${currentDay}/${currentMonth}/${currentYear}`)
                }
                else if (inputMonth == 2 && inputDay >= 29) {
                    if (inputDay == 29) {
                        currentDay = 1;
                        currentMonth++;
                        addTextById("txt-dateResult", `${currentDay}/${currentMonth}/${currentYear}`)
                    }
                    else {
                        addTextById("txt-dateResult", "Ngày không xác định")
                    }
                }
                else if (inputDay <= lastDayOfMonth) {
                    if (inputDay == lastDayOfMonth) {
                        currentDay = 1;
                        currentMonth++;
                    }
                    else {
                        currentDay++;
                    }
                    addTextById("txt-dateResult", `${currentDay}/${currentMonth}/${currentYear}`)
                }


            }

            break;

        case false:
            if (inputDay <= 0 || inputDay >= 32 || inputMonth <= 0 || inputMonth > 12 || inputYear <= 0) {
                checkValidDate(inputDay, inputMonth, inputYear)
            }
            else {
                if (inputDay > lastDayOfMonth) {
                    addTextById("txt-dateResult", "Ngày không xác định")
                }
                else if (inputDay == lastDayOfMonth && inputMonth == 12) {
                    currentDay = 1;
                    currentMonth = 1;
                    currentYear++;
                    addTextById("txt-dateResult", `${currentDay}/${currentMonth}/${currentYear}`)
                }
                else if (inputMonth == 2 && inputDay >= 28) {
                    if (inputDay == 28) {
                        currentDay = 1;
                        currentMonth++;
                        addTextById("txt-dateResult", `${currentDay}/${currentMonth}/${currentYear}`)
                    }
                    else {
                        addTextById("txt-dateResult", "Ngày không xác định")
                    }
                }
                else if (inputDay <= lastDayOfMonth) {
                    if (inputDay == lastDayOfMonth) {
                        currentDay = 1;
                        currentMonth++;
                    }
                    else {
                        currentDay++;
                    }
                    addTextById("txt-dateResult", `${currentDay}/${currentMonth}/${currentYear}`)
                }


            }

            break;
    }




}

function previousDateCalculate() {
    var inputDay = document.getElementById("inputDay").value * 1;
    var inputMonth = document.getElementById("inputMonth").value * 1;
    var inputYear = document.getElementById("inputYear").value * 1;
    var lastDayOfMonth = getNumberOfDaysFromMonth(inputMonth, inputYear)
    var lastDayOfPreViousMonth = getNumberOfDaysFromMonth(inputMonth - 1, inputYear)
    console.log('lastDayOfPreViousMonth: ', lastDayOfPreViousMonth);
    var currentDay = inputDay;
    var currentMonth = inputMonth;
    var currentYear = inputYear;

    switch (isLeapYear(inputYear)) {
        case true:
            if (inputDay <= 0 || inputDay >= 32 || inputMonth <= 0 || inputMonth > 12 || inputYear <= 0) {
                checkValidDate(inputDay, inputMonth, inputYear)
            }
            else {
                if (inputDay > lastDayOfMonth) {
                    addTextById("txt-dateResult", "Ngày không xác định")
                }
                else if (inputDay == 1 && inputMonth == 1) {
                    currentDay = 31;
                    currentMonth = 12;
                    currentYear--;
                    addTextById("txt-dateResult", `${currentDay}/${currentMonth}/${currentYear}`)
                }
                else if (inputMonth == 2 && inputDay > 29) {
                    addTextById("txt-dateResult", `Ngày không xác định`)
                }
                else if (inputDay == 1) {
                    currentDay = lastDayOfPreViousMonth;
                    currentMonth--;
                    addTextById("txt-dateResult", `${currentDay}/${currentMonth}/${currentYear}`)
                }
                else {
                    currentDay--;
                    addTextById("txt-dateResult", `${currentDay}/${currentMonth}/${currentYear}`)
                }
            }

            break;

        case false:
            if (inputDay <= 0 || inputDay >= 32 || inputMonth <= 0 || inputMonth > 12 || inputYear <= 0) {
                checkValidDate(inputDay, inputMonth, inputYear)
            }
            else {
                if (inputDay > lastDayOfMonth) {
                    addTextById("txt-dateResult", "Ngày không xác định")
                }
                else if (inputDay == 1 && inputMonth == 1) {
                    currentDay = 31;
                    currentMonth = 12;
                    currentYear--;
                    addTextById("txt-dateResult", `${currentDay}/${currentMonth}/${currentYear}`)
                }
                else if (inputMonth == 2 && inputDay > 28) {
                    addTextById("txt-dateResult", `Ngày không xác định`)
                }
                else if (inputDay == 1) {
                    currentDay = lastDayOfPreViousMonth;
                    currentMonth--;
                    addTextById("txt-dateResult", `${currentDay}/${currentMonth}/${currentYear}`)
                }
                else {
                    currentDay--;
                    addTextById("txt-dateResult", `${currentDay}/${currentMonth}/${currentYear}`)
                }
            }

            break;
    }






}

function showNumberOfDaysFromMonth() {
    var inputMonth2 = document.getElementById("inputMonth2").value * 1;
    var inputYear2 = document.getElementById('inputYear2').value * 1;
    var numberOfDays = getNumberOfDaysFromMonth(inputMonth2, inputYear2);

    addTextById("txt-showDateFromMonth", `Tháng ${inputMonth2} năm ${inputYear2} có ${numberOfDays} ngày`)
}

function readNumber() {
    var readNumber = document.getElementById("inputNumberRead").value * 1;
    var hundredsNumber = Math.floor(readNumber / 100);
    var tensNumber = Math.floor((readNumber / 10) % 10);
    var unitNumber = Math.floor((readNumber % 100) % 10);
    var readHundredsNumber;
    var readTensNumber;
    var readUnitNumber;
    if (readNumber > 999 || readNumber <= 0) {
        addTextById("txt-readNumber", "Hãy nhập số dương có 3 chữ số")
    }
    else if (hundredsNumber == 0 || tensNumber == 0 || unitNumber == 0) {
        if (hundredsNumber == 0) {
            addTextById("txt-readNumber", "Hàng trăm không xác định")
        }
        else if (tensNumber == 0) {
            addTextById("txt-readNumber", "Hàng chục không xác định")
        }
        else {
            addTextById("txt-readNumber", "Hàng đơn vị không xác định")
        }
    }
    else {
        switch (hundredsNumber) {
            case 1:
                readHundredsNumber = "Một"
                break;
            case 2:
                readHundredsNumber = "Hai"
                break;
            case 3:
                readHundredsNumber = "Ba"
                break;
            case 4:
                readHundredsNumber = "Bốn"
                break;
            case 5:
                readHundredsNumber = "Năm"
                break;
            case 6:
                readHundredsNumber = "Sáu"
                break;
            case 7:
                readHundredsNumber = "Bảy"
                break;
            case 8:
                readHundredsNumber = "Tám"
                break;
            case 9:
                readHundredsNumber = "Chín"
                break;

        }
        switch (tensNumber) {
            case 1:
                readTensNumber = "mười"
                break;
            case 2:
                readTensNumber = "hai mươi"
                break;
            case 3:
                readTensNumber = "ba mươi"
                break;
            case 4:
                readTensNumber = "bốn mươi"
                break;
            case 5:
                readTensNumber = "năm mươi"
                break;
            case 6:
                readTensNumber = "sáu mươi"
                break;
            case 7:
                readTensNumber = "bảy mươi"
                break;
            case 8:
                readTensNumber = "tám mươi"
                break;
            case 9:
                readTensNumber = "chín mươi"
                break;

        }
        switch (unitNumber) {
            case 1:
                readUnitNumber = "mốt"
                break;
            case 2:
                readUnitNumber = "hai"
                break;
            case 3:
                readUnitNumber = "ba"
                break;
            case 4:
                readUnitNumber = "bốn"
                break;
            case 5:
                readUnitNumber = "lăm"
                break;
            case 6:
                readUnitNumber = "sáu"
                break;
            case 7:
                readUnitNumber = "bảy"
                break;
            case 8:
                readUnitNumber = "tám"
                break;
            case 9:
                readUnitNumber = "chín"
                break;
        }

        if (tensNumber == 1 && unitNumber == 1) {
            readUnitNumber = "một";
            addTextById("txt-readNumber", `${readHundredsNumber} trăm ${readTensNumber}  ${readUnitNumber}`)
        }
        else if (unitNumber == 1) {
            readUnitNumber = "mốt";
            addTextById("txt-readNumber", `${readHundredsNumber} trăm ${readTensNumber}  ${readUnitNumber}`)
        }
        else {
            addTextById("txt-readNumber", `${readHundredsNumber} trăm ${readTensNumber}  ${readUnitNumber}`)
        }

    }



}

function lineSegmentLenghtCalculate(x1, y1, x2, y2) {
    var lenght = Math.sqrt((Math.pow((x2 - x1), 2)) + (Math.pow((y2 - y1), 2)))
    return lenght;

}



function findFurthestPerson() {
    var inputName1 = document.getElementById("inputName1").value;
    var inputX1 = document.getElementById("inputX1").value * 1;
    var inputY1 = document.getElementById("inputY1").value * 1;
    var inputName2 = document.getElementById("inputName2").value;
    var inputX2 = document.getElementById("inputX2").value * 1;
    var inputY2 = document.getElementById("inputY2").value * 1;
    var inputName3 = document.getElementById("inputName3").value;
    var inputX3 = document.getElementById("inputX3").value * 1;
    var inputY3 = document.getElementById("inputY3").value * 1;

    var schoolInputX = document.getElementById("inputX4").value * 1;
    var schoolInputY = document.getElementById("inputY4").value * 1;


    var inputName1Distance = lineSegmentLenghtCalculate(inputX1, inputY1, schoolInputX, schoolInputY);
    var inputName2Distance = lineSegmentLenghtCalculate(inputX2, inputY2, schoolInputX, schoolInputY);
    var inputName3Distance = lineSegmentLenghtCalculate(inputX3, inputY3, schoolInputX, schoolInputY);

    var [, , furthestDistance] = sortThreeNumbers(inputName1Distance, inputName2Distance, inputName3Distance);
    var furthestPerson;

    switch (furthestDistance) {
        case inputName1Distance:
            furthestPerson = inputName1
            break;
        case inputName2Distance:
            furthestPerson = inputName2
            break;
        case inputName3Distance:
            furthestPerson = inputName3;
            break;
    }

    // if (furthestDistance == inputName1Distance){
    //     furthestPerson = inputName1;
    // } 
    // else if (furthestDistance == inputName2Distance){
    //     furthestPerson = inputName2;
    // }
    // else  {
    //     furthestPerson = inputName3;
    // }

    addTextById("txtSearch", `Sinh viên xa trường nhất: ${furthestPerson}`)


}
