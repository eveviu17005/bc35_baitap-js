function addTextByID(id,text){
    document.getElementById(id).innerText = text;
}
function failPassChecking() {
    var benchMark = document.getElementById("inputScore1").value*1;
    var inputScore1 = document.getElementById("inputScore2").value*1;
    var inputScore2 = document.getElementById("inputScore3").value*1;
    var inputScore3 = document.getElementById("inputScore4").value*1;
     var areaValue = document.getElementById("selLocation");
    var areaScore = areaValue.options[areaValue.selectedIndex].value*1;
    var beneficiaryValue = document.getElementById("selUser");
    var beneficiaryScore = beneficiaryValue.options[beneficiaryValue.selectedIndex].value*1;
    var totalScore = (inputScore1 + inputScore2 + inputScore3) + areaScore + beneficiaryScore;
  
    

    if (inputScore1 <= 0|| inputScore2 <= 0 || inputScore3 <= 0) {
        addTextByID("txtResult","Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0")
    }
    else if (totalScore >= benchMark){
        addTextByID("txtResult",`Bạn đã đậu. Tổng điểm: ${totalScore}`)
        }
    else {
        addTextByID("txtResult",`Bạn đã rớt. Tổng điểm: ${totalScore}` )
    
    }

}

const FIRST_50kW_PRICE = 500;
const FROM_51KW_TO_100KW_PRICE = 650;
const FROM_101KW_TO_200KW_PRICE = 850;
const FROM_201KW_TO_350KW_PRICE = 1100;
const MORE_THAN_350KW_PRICE = 1300;


function getElectricBill(inputKw){
    var total = 0; 
    var first50Kw = inputKw *FIRST_50kW_PRICE;
    var from51To100Kw = (50*FIRST_50kW_PRICE) + (inputKw - 50)*FROM_51KW_TO_100KW_PRICE;
    var from101To200Kw = (50*FIRST_50kW_PRICE) + (50*FROM_51KW_TO_100KW_PRICE) + (inputKw - 100)*FROM_101KW_TO_200KW_PRICE
    var from201To350Kw = (50*FIRST_50kW_PRICE) + (50*FROM_51KW_TO_100KW_PRICE) + (100*FROM_101KW_TO_200KW_PRICE) + (inputKw - 200)*FROM_201KW_TO_350KW_PRICE;
    var moreThan350Kw = (50* FIRST_50kW_PRICE) + (50*FROM_51KW_TO_100KW_PRICE) + (100*FROM_101KW_TO_200KW_PRICE) + (150*FROM_201KW_TO_350KW_PRICE) + (inputKw - 350)*MORE_THAN_350KW_PRICE
    
    if  (inputKw >= 0 && inputKw <= 50) {
        total = first50Kw;
    }
    else if (inputKw > 50 &&  inputKw <= 100){
        total = from51To100Kw;
    }
    else if (inputKw > 100 && inputKw <= 200){
        total = from101To200Kw;
    }
    else if (inputKw > 200 && inputKw <= 350 ){
        total = from201To350Kw;
    }
    else  {
        total = moreThan350Kw;
    }
    return total

}

function showElectricBill(){
    var inputName = document.getElementById("inputName").value;
    var inputKW = document.getElementById("inputKW").value*1;
    var totalBill =  new Intl.NumberFormat('vn-VN').format(getElectricBill(inputKW)) ;
    if (inputKW < 0){
        addTextByID("txtElecBill","Số Kw không hợp lệ! Vui lòng nhập lại")
    }
    else {
        addTextByID("txtElecBill",`Họ tên: ${inputName}; Tiền điện ${totalBill}`)
    }

    
}

function taxCalculating() {
    var taxPayer = document.getElementById("inputName2").value;
    var inputSalary = document.getElementById("inputSalary").value*1;
    var inputUserAmount = document.getElementById("inputUser").value*1;
    var taxableIncome = inputSalary - (4e+6) - (inputUserAmount*1.6e+6);
    var totalTax=0;

    if (taxableIncome <= 60e+6){
        totalTax = taxableIncome*0.05;
    }
    else if (taxableIncome > 60e+6 && taxableIncome <=120e+6) {
        totalTax = taxableIncome*0.1;
    }
    else if (taxableIncome>120e+6 && taxableIncome<=210e+6){
        totalTax = taxableIncome*0.15;
    }
    else if (taxableIncome>210e+6 && taxableIncome<=384e+6){
        totalTax = taxableIncome*0.2;
    }
    else if (taxableIncome>384e+6 && taxableIncome<=624e+6) {
        totalTax = taxableIncome*0.25;
    }
    else if (taxableIncome>624e+6 && taxableIncome<=960e+6){
        totalTax = taxableIncome*0.3;
    }
    else if (taxableIncome>960e+6){
        totalTax = taxableIncome*0.35;
    }

    totalTax =new Intl.NumberFormat('vn-VN').format(totalTax);
    addTextByID("txtTax",`Họ tên: ${taxPayer}; Tiền thuế thu nhập cá nhân: ${totalTax}`)

}

function disableInput() {
    var selCustomerValue = document.getElementById("selCustomer").value;
    var inputConnect = document.getElementById("inputConnect");
    if (selCustomerValue == "company") {
        inputConnect.style.display = "block"
    }
    else {
        inputConnect.style.display = "none"
    }
}

function getCableBill(){
    var selCustomerValue = document.getElementById("selCustomer").value;
    var cusomerId = document.getElementById("inputID").value;
    var premiumChannelAmount = document.getElementById("inputChanel").value*1;
    var companyConnectAmount = document.getElementById("inputConnect").value*1;
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
      });
    
    var serviceCharge = 0;
    var premiumUnitPrice = 0;

    var processingFee = 0;
    var total = 0;

    if (premiumChannelAmount<0 || companyConnectAmount <0){
        addTextByID("btnNet","Số lượng kênh - kết nối phải lớn hơn hoặc bằng 0")
    }
    else {
        switch(selCustomerValue){
            case "user":
                serviceCharge = 20.5;
                processingFee = 4.5;
                premiumUnitPrice = 7.5
                premiumTotalFee = premiumUnitPrice *premiumChannelAmount;
                
                total = formatter.format(serviceCharge + processingFee + premiumTotalFee)
                addTextByID("txtNet",`Mã khách hàng ${cusomerId}; Tiền cáp: ${total}`)
                break;
    
            case "company":
                processingFee = 15;
                premiumUnitPrice = 50;
                premiumTotalFee = premiumUnitPrice *premiumChannelAmount;
                if (companyConnectAmount <= 10){
                    serviceCharge = 75;
                }
                else {
                    serviceCharge = 75 + (companyConnectAmount -10)*5;
                }
                total = formatter.format(serviceCharge + processingFee + premiumTotalFee)
                addTextByID("txtNet",`Mã khách hàng ${cusomerId}; Tiền cáp: ${total}`)
                break;
            case "":
                addTextByID("txtNet","Hãy chọn loại khách hàng")
                break;
        }
    }



    

}