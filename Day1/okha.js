/** BÀI 1: TÍNH TIỀN LƯƠNG
 *
 * Đầu vào:
 * + Lương một ngày: 100 000
 * + Số ngày làm việc: 24
 *
 * Các bước xử lý:
 * + Bước 1: Tạo 2 biến: dailyWage, workingDays
 * + Bước 2: Gán giá trị cho dailyWage, workingDays lần lượt là 100000, 24
 * + Bước 3: Tạo biến salary
 * + Bước 4: Gán giá trị cho biến salary bằng công thức tính lương của đề bài
 * => salary = dailyWage * workingDays
 * + Bước  5: in kết quả ra console
 *
 * Đầu ra:
 * + Tiền lương: 2400000
 *
 */

var dailyWage = 100000;
var workingDays = 24;

var salary = dailyWage * workingDays;

console.log("Tổng tiền lương:", salary);

/** Bài 2: Tính giá trị trung bình
 *
 * Đầu vào:
 * + Số thứ nhất: 23
 * + Số thứ 2: 7
 * + Số  thứ 3: 1999
 * + Số thứ 4: 3
 * + Số thứ 5: 8
 *
 * Các bước xử lý:
 * + Bước 1: Tạo 5 biến firstNumber, secondNumber, thirdNumber, fourthNumber, fifthNumber
 * + Bước 2: Gán giá trị cho firstNumber, secondNumber, thirdNumber, fourthNumber, fifthNumber lần lược là 23, 7, 1999, 3, 8
 * + Bước 3: Tạo biến averageNumber
 * + Bước 4: Gán giá trị cho averageNumber bằng công thức tính số trung bình:
 * => averageNumber = (firstNumber + secondNumber + thirdNumber + fourthNumber + fifthNumber)/5
 * + Bước 5: in kết quả ra console
 *
 * Đầu ra:
 * Giá trị trung bình: 408
 */

var firstNumber = 23;
var secondNumber = 7;
var thirdNumber = 1999;
var fourthNumber = 3;
var fifthNumber = 8;

var averageNumber =
  (firstNumber + secondNumber + thirdNumber + fourthNumber + fifthNumber) / 5;

console.log("Giá trị trung bình:", averageNumber);

/** Bài 3: Quy đổi tiền
 *
 * Đầu vào:
 * Tỉ giá USD: 23 500
 * Số lượng USD muốn đổi: 275000
 *
 * Các bước xử lý:
 * + Bước 1: tạo 2 biến usdRate, usdAmount
 * + Bước 2: gán giá trị 23500 cho usdRate, 275000 cho usdAmount
 * + Bước 3: Tạo biến vietnamDong
 * + Bước 4: gán giá trị cho vietnamDong bằng công thức:
 * Số VND = tỉ giá * số lượng USD muốn đổi
 * => vietnamDong = usdRate * usdAmount
 * + Bước 5: in kết quả ra console
 *
 * Đầu ra:
 *
 * Số VND sau khi đổi: 6462500000
 *
 *  */


var usdRate = 23500;
var usdAmount = 275000;

var vietnamDong = usdRate * usdAmount;

console.log("Thành tiền VND:", vietnamDong);

/** Bài 4: TÍNH DIỆN TÍCH, CHU VI HÌNH CHỮ NHẬT
 *
 * Đầu vào:
 * + Chiều dài: 23
 * + Chiều rộng: 7
 *
 * Các bước xử lý:
 * + Bước 1: tạo 2 biến: rectangleLength, rectangleBreadth
 * + Bước 2: gán 23 cho rectangleLength, 7 cho rectangleBreadth
 * + Bước 3: tạo 2 biến: rectangleArea, rectanglePerimeter
 * + Bước 4: gán giá trị cho rectangleArea, rectanglePerimeter bằng công thức tính diện tích, chu vi hình chữ nhật.
 * Diện tích = chiều dài * chiều rộng
 * => rectangleArea = rectangleLength * rectangleBreadth
 * Chu vi = (chiều dài + chiều rộng)*2
 * =>  rectanglePerimeter = (rectangleLength + rectangleBreadth)*2
 * + Bước 5: in kết quả ra console
 *
 * Đầu ra:
 * Diện tích: 161
 * Chu vi: 60
 */

var rectangleLength = 23;
var rectangleBreadth = 7;

var rectangleArea = rectangleLength * rectangleBreadth;
var rectanglePerimeter = (rectangleLength + rectangleBreadth) * 2;

console.log("Chu vi:", rectangleArea);
console.log("Diện tích:", rectanglePerimeter);

/**BÀI 5: TÍNH TỔNG 2 KÝ SỐ
 *
 * Đầu vào:
 * + Số tự nhiên có 2 chữ số: 23
 *
 * Các bước xử lý:
 * + Bước 1: tạo biến baseNumber
 * + Bước 2: gán giá trị 23 cho baseNumber
 * + Bước 3: tạo 2 biến unitNumber, tensNumber
 * + Bước 4: Gán giá trị cho uniNumber, tensNumber bằng:
 * Tách số hàng chục theo công thức:
 * => tensNumber = Math.floor(baseNumber / 10);
 * Tách số hàng đơn vị theo công thức:
 * => unitNumber = baseNumber % 10;
 * + Bước 5: tạo biến Sum
 * + Bước 6: Gán giá trị cho Sum bằng cách cộng số hàng chục và hàng đơn vị:
 * => Sum = tensNumber + unitNumber;
 * + Bước 7: in kết quả ra console
 *
 * Đầu ra:
 *
 * Tổng 2 ký số: 5
 *
 */

var baseNumber = 23;
var unitNumber = baseNumber % 10;
var tensNumber = Math.floor(baseNumber / 10);

var Sum = tensNumber + unitNumber;

console.log("Tổng 2 ký số:", Sum);
